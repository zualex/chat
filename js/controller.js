/**
 * Created by alexanderzulechner on 19.10.15.
 */

var app = angular.module('CPB_Chat', []);

app.controller('CPB_Chat_Ctrl', function ($scope, $log) {

    $scope.createMessage = function () {
        var obj = new Object();
        //if message exists

        if (!$scope.newMessage) {
            alert("Nachricht eingeben!");
            return false;
        } else {
            obj.body = $scope.newMessage;
        }
        if (!$scope.person) {
            alert("Personenname eingeben!");
            return false;
        } else {
            obj.person = $scope.person;
        }
        if (!$scope.color) {
            alert("Farbe eingeben!");
        } else {
            obj.color = $scope.color;
        }
        $scope.pushMessage(obj);
        $scope.messages = $scope.getMessages();
    };
    $scope.pushMessage = function (obj) {
        var messages;
        if (typeof(Storage) !== "undefined") {
            messages = $scope.getMessages();
            if (!messages) {
                messages = [];
            }
            messages.push(obj);
            localStorage.setItem('chat', JSON.stringify(messages));
        } else {
            alert("No local Storage Support!");
        }
    };
    $scope.getMessages = function () {
        var messages = [];
        messages = JSON.parse(localStorage.getItem("chat"));
        return messages;
    };

    $scope.isChat = true;
    $scope.messages = $scope.getMessages();
});
